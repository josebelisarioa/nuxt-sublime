module.exports = {
  root: true,
  env: {
    browser: true,
    node: true
  },
  globals: {
    "$": true,
    "jQuery": true
  },
  parserOptions: {
    parser: 'babel-eslint'
  },
  extends: [
    'prettier',
    'prettier/vue',
    'plugin:prettier/recommended',
    '@nuxtjs',
    'plugin:nuxt/recommended'
  ],
  plugins: ['prettier'],
  rules: {
    'vue/html-self-closing': 0,
    'space-before-function-paren': 0,
    'arrow-parens': 0,
    'no-multiple-empty-lines': 0,
    'no-console': 'off',
    'no-extend-native': 0
  }
}
