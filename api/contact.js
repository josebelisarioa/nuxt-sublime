import express from 'express'
import nodemailer from 'nodemailer'
import validator from 'validator'
import xssFilters from 'xss-filters'

const USER = 'sublimeartestudio@gmail.com'
const PASSWORD = 'Perfe18ction*'
const TO = 'jose_belisario@live.com' // 'hello@sublimestudio.la'

const app = express()
app.use(express.json())

app.post('/', (req, res) => {
  try {
    const attributes = ['name', 'email', 'subject', 'budget', 'dueDate']
    const sanitizedAttributes = attributes.map((n) =>
      validateAndSanitize(n, req.body[n])
    )

    const someInvalid = sanitizedAttributes.some((r) => !r)

    if (someInvalid) {
      console.log(req.body, sanitizedAttributes)
      return res.status(422).json({ error: 'Ugh.. That looks unprocessable!' })
    }

    sendMail(...sanitizedAttributes)
    res.status(200).json({ message: 'Email sent' })
  } catch {
    res.status(500)
  }
})

const validateAndSanitize = (key, value) => {
  return (
    rejectFunctions.has(key) &&
    !rejectFunctions.get(key)(value) &&
    xssFilters.inHTMLData(value)
  )
}

const rejectFunctions = new Map([
  ['name', (v) => v.lenght < 4],
  ['email', (v) => !validator.isEmail(v)],
  ['subject', (v) => v.length < 1],
  ['budget', () => false],
  ['dueDate', () => false]
])

const sendMail = (name, email, subject, budget, dueDate) => {
  const transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
      user: USER,
      pass: PASSWORD
    }
  })
  transporter.sendMail({
    from: 'Contact <sublimeartestudio@gmail.com>',
    replyTo: email,
    to: TO,
    subject: `New contact from ${name}`,
    html: `
    <strong>Message from:</strong> ${name}
    <br><br>
    <strong>Subject:</strong>
    <p>${subject}</p>
    <br>
    <strong>Budget:</strong> ${budget}
    <br>
    <strong>Due date:</strong> ${dueDate}
    `
  })
}

export default {
  path: '/api/contact',
  handler: app
}
