const preloadProject = (project, callback) => {
  if (project.loaded >= project.urls.length) {
    if (callback) callback()
  } else {
    project.urls.forEach((src) => {
      const img = new Image()
      img.onload = () => addOne(project, callback)
      img.onError = () => onError(src, project, callback)
      img.src = src
    })
  }
}

const addOne = (project, callback) => {
  project.loaded++
  if (project.loaded >= project.urls.length) {
    if (callback) callback()
  }
}

const onError = (src, project, callback) => {
  console.log(`src ${src} might not exists`)
  addOne(project, callback)
}

export default preloadProject
