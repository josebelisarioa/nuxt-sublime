export default [
  {
    title: '@psico_consciencia',
    subtitle: 'Money Priest',
    position: {
      x: 40,
      y: 390,
      w: 170,
      h: 590
    },
    url: 'https://www.instagram.com/psico_consciencia/',
    img: require('@/assets/images/about/t_alix.png'),
    textLocation: {
      x: 150,
      y: 420,
      w: 235
    }
  },
  {
    title: '@gemelocha',
    subtitle: 'Digital Goldsmith / Designer',
    position: {
      x: 505,
      y: 270,
      w: 635,
      h: 470
    },
    url: 'https://www.instagram.com/gemelocha/',
    img: require('@/assets/images/about/t_maria.png'),
    textLocation: {
      x: 636,
      y: 310,
      w: 175
    }
  },
  {
    title: '@adelsodavidr',
    subtitle: `Oil's Wizard / Painter`,
    position: {
      x: 635,
      y: 130,
      w: 765,
      h: 330
    },
    url: 'https://www.instagram.com/adelsodavidr/',
    img: require('@/assets/images/about/t_adelso.png'),
    textLocation: {
      x: 744,
      y: 170,
      w: 132
    }
  },
  {
    title: '@hweffer',
    subtitle: 'Stonemason / 3D Generalist',
    position: {
      x: 790,
      y: 310,
      w: 930,
      h: 510
    },
    url: 'https://www.instagram.com/hweffer/',
    img: require('@/assets/images/about/t_hender.png'),
    textLocation: {
      x: 926,
      y: 360,
      w: 169
    }
  },
  {
    title: '@luisito.enlinea',
    subtitle: 'Concepts Tailor / Designer',
    position: {
      x: 930,
      y: 195,
      w: 1060,
      h: 395
    },
    url: 'https://www.instagram.com/luisito.enlinea/',
    img: require('@/assets/images/about/t_luis.png'),
    textLocation: {
      x: 1060,
      y: 220,
      w: 226
    }
  },
  {
    title: '@lagemelahippie',
    subtitle: 'Watercolor Crafter / Painter',
    position: {
      x: 1105,
      y: 290,
      w: 1235,
      h: 490
    },
    url: 'https://www.instagram.com/lagemelahippie/',
    img: require('@/assets/images/about/t_mariangel.png'),
    textLocation: {
      x: 1270,
      y: 330,
      w: 229
    }
  },
  {
    title: '@stowergd',
    subtitle: 'Lord of the pen / Illustrator',
    position: {
      x: 1245,
      y: 100,
      w: 1375,
      h: 300
    },
    url: 'https://www.instagram.com/stow01/',
    img: require('@/assets/images/about/t_andres.png'),
    textLocation: {
      x: 1385,
      y: 125,
      w: 174
    }
  },
  {
    title: '@josebelisario',
    subtitle: 'Dark Magician / Developer',
    position: {
      x: 1495,
      y: 205,
      w: 1625,
      h: 405
    },
    url: 'https://www.instagram.com/josebelisario/',
    img: require('@/assets/images/about/t_jose.png'),
    textLocation: {
      x: 1640,
      y: 235,
      w: 202
    }
  }
]
