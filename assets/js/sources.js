export default {
  process: {
    loaded: 0,
    urls: [
      // Glitch
      require('@/assets/images/layout/glitch_1_process.png'),
      require('@/assets/images/layout/glitch_2_process.png'),
      require('@/assets/images/layout/glitch_3_process.png'),
      require('@/assets/images/layout/glitch_4_process.png'),
      require('@/assets/images/layout/glitch_5_process.png'),
      require('@/assets/images/layout/glitch_6_process.png'),
      // About
      require('@/assets/images/about/process_base.jpg'),
      require('@/assets/images/about/process_uv.png'),
      require('@/assets/images/about/candle_process.png'),
      require('@/assets/images/about/light_process.png')
    ]
  },
  results: {
    loaded: 0,
    urls: [
      // Glitch
      require('@/assets/images/layout/glitch_1_results.png'),
      require('@/assets/images/layout/glitch_2_results.png'),
      require('@/assets/images/layout/glitch_3_results.png'),
      require('@/assets/images/layout/glitch_4_results.png'),
      require('@/assets/images/layout/glitch_5_results.png'),
      require('@/assets/images/layout/glitch_6_results.png'),
      // About
      require('@/assets/images/about/results_base.jpg'),
      require('@/assets/images/about/results_uv.png'),
      require('@/assets/images/about/candle_results.png'),
      require('@/assets/images/about/light_results.png')
    ]
  },
  aboutText: {
    loaded: 0,
    urls: [
      require('@/assets/images/about/t_alix.png'),
      require('@/assets/images/about/t_maria.png'),
      require('@/assets/images/about/t_adelso.png'),
      require('@/assets/images/about/t_hender.png'),
      require('@/assets/images/about/t_luis.png'),
      require('@/assets/images/about/t_mariangel.png'),
      require('@/assets/images/about/t_andres.png'),
      require('@/assets/images/about/t_jose.png')
    ]
  },
  eightTree: {
    loaded: 0,
    urls: [
      // Section 2
      require('@/assets/images/eight-tree/8tree_island.png'),
      require('@/assets/images/eight-tree/3d.png'),
      require('@/assets/images/eight-tree/sec2t1.png'),
      require('@/assets/images/eight-tree/sec2t2.png'),
      require('@/assets/images/eight-tree/3d_for_child.png'),
      // Section 3
      require('@/assets/images/eight-tree/beginning.png'),
      require('@/assets/images/eight-tree/character.png'),
      require('@/assets/images/eight-tree/sec3t1.png'),
      require('@/assets/images/eight-tree/shark_draft.png'),
      require('@/assets/images/eight-tree/from_w.png'),
      require('@/assets/images/eight-tree/pc1.jpg'),
      require('@/assets/images/eight-tree/pc2.jpg'),
      require('@/assets/images/eight-tree/pc3.jpg'),
      require('@/assets/images/eight-tree/pc4.jpg'),
      require('@/assets/images/eight-tree/pc5.jpg'),
      require('@/assets/images/eight-tree/pc6.jpg'),
      require('@/assets/images/eight-tree/pc7.jpg'),
      // Section 4
      require('@/assets/images/eight-tree/ti1.jpg'),
      require('@/assets/images/eight-tree/ti2.jpg'),
      require('@/assets/images/eight-tree/ti3.jpg'),
      require('@/assets/images/eight-tree/ti4.jpg'),
      require('@/assets/images/eight-tree/ti5.jpg'),
      require('@/assets/images/eight-tree/ti6.jpg'),
      require('@/assets/images/eight-tree/ti7.jpg'),
      require('@/assets/images/eight-tree/ti8.jpg'),
      // Section 5
      require('@/assets/images/eight-tree/cp1.jpg'),
      require('@/assets/images/eight-tree/cp2.jpg'),
      require('@/assets/images/eight-tree/cp3.jpg'),
      require('@/assets/images/eight-tree/cp4.jpg'),
      require('@/assets/images/eight-tree/cp5.jpg'),
      require('@/assets/images/eight-tree/cp6.jpg'),
      require('@/assets/images/eight-tree/cp7.jpg'),
      // Section 6
      require('@/assets/images/eight-tree/61.jpg'),
      require('@/assets/images/eight-tree/62.jpg'),
      require('@/assets/images/eight-tree/63.jpg'),
      require('@/assets/images/eight-tree/64.jpg'),
      require('@/assets/images/eight-tree/65.jpg'),
      require('@/assets/images/eight-tree/66.jpg'),
      require('@/assets/images/eight-tree/67.jpg'),
      // Section 7
      require('@/assets/images/eight-tree/the_character.png'),
      require('@/assets/images/eight-tree/sec7t1.png'),
      require('@/assets/images/eight-tree/bubb.png'),
      require('@/assets/images/eight-tree/tcdraft.png'),
      require('@/assets/images/eight-tree/2018_y.png'),
      require('@/assets/images/eight-tree/cute_panda_boy.png'),
      require('@/assets/images/eight-tree/arrow_left.png'),
      require('@/assets/images/eight-tree/arrow_right.png'),
      require('@/assets/images/eight-tree/71.png'),
      require('@/assets/images/eight-tree/72.png'),
      require('@/assets/images/eight-tree/73.png'),
      require('@/assets/images/eight-tree/74.png'),
      require('@/assets/images/eight-tree/75.png'),
      // Section 8
      require('@/assets/images/eight-tree/the_char_world.png'),
      require('@/assets/images/eight-tree/bubbas_bedroom.png'),
      require('@/assets/images/eight-tree/sec8t1_1.png'),
      require('@/assets/images/eight-tree/scene.png'),
      require('@/assets/images/eight-tree/8tree_world.png'),
      require('@/assets/images/eight-tree/81.jpg'),
      require('@/assets/images/eight-tree/82.jpg'),
      require('@/assets/images/eight-tree/83.jpg'),
      require('@/assets/images/eight-tree/84.jpg'),
      require('@/assets/images/eight-tree/85.jpg'),
      require('@/assets/images/eight-tree/91-trim.png'),
      require('@/assets/images/eight-tree/92-trim.png'),
      require('@/assets/images/eight-tree/93-trim.png'),
      require('@/assets/images/eight-tree/94-trim.png'),
      require('@/assets/images/eight-tree/95-trim.png'),
      require('@/assets/images/eight-tree/101.png'),
      require('@/assets/images/eight-tree/102.png'),
      require('@/assets/images/eight-tree/103.png'),
      require('@/assets/images/eight-tree/104.png'),
      require('@/assets/images/eight-tree/105.png'),
      require('@/assets/images/eight-tree/106.png')
    ]
  },
  drco: {
    loaded: 0,
    urls: [
      // Section 2
      require('@/assets/images/drco/2019.png'),
      require('@/assets/images/drco/palms_mapdegrad.png'),
      require('@/assets/images/drco/78.png'),
      require('@/assets/images/drco/79.png'),
      require('@/assets/images/drco/points_page2.png'),
      // Section 3
      require('@/assets/images/drco/logo_brand_emblem.png'),
      require('@/assets/images/drco/variations.png'),
      require('@/assets/images/drco/visual_identity_applications.png'),
      require('@/assets/images/drco/lines_red_white.png'),
      require('@/assets/images/drco/decor_points_mark_variations.png'),
      require('@/assets/images/drco/88.png'),
      // Section 4
      require('@/assets/images/drco/patterns.png'),
      require('@/assets/images/drco/patterns_info.png'),
      // Section 11
      require('@/assets/images/drco/points_page_a.png'),
      require('@/assets/images/drco/process.png'),
      require('@/assets/images/drco/modeboard.png'),
      require('@/assets/images/drco/31.png'),
      // Section 12
      require('@/assets/images/drco/points_page.png'),
      require('@/assets/images/drco/33.png'),
      require('@/assets/images/drco/34.png'),
      require('@/assets/images/drco/35.png'),
      require('@/assets/images/drco/37.png'),
      require('@/assets/images/drco/36.png'),
      require('@/assets/images/drco/2lines.png'),
      // Section 13
      require('@/assets/images/drco/timon_mobile.png'),
      require('@/assets/images/drco/44.png'),
      require('@/assets/images/drco/43.png'),
      require('@/assets/images/drco/41.png'),
      require('@/assets/images/drco/circle.png'),
      // Large Images
      require('@/assets/images/drco/modeboard.png'),
      require('@/assets/images/drco/wallpaper_pieces.png'),
      require('@/assets/images/drco/1.png'),
      // Sketchs
      require('@/assets/images/drco/sketch1.png'),
      require('@/assets/images/drco/sketch2.png'),
      require('@/assets/images/drco/sketch3.png'),
      require('@/assets/images/drco/sketch4.png'),
      require('@/assets/images/drco/sketch5.png'),
      require('@/assets/images/drco/sketch6.png'),
      require('@/assets/images/drco/colors1.png'),
      require('@/assets/images/drco/colors2.png'),
      require('@/assets/images/drco/colors3.png'),
      require('@/assets/images/drco/colors4.png'),
      require('@/assets/images/drco/colors5.png'),
      require('@/assets/images/drco/colors6.png'),
      // Desings
      require('@/assets/images/drco/girl1.png'),
      require('@/assets/images/drco/girl2.png'),
      require('@/assets/images/drco/girl3.png'),
      require('@/assets/images/drco/girl4.png'),
      require('@/assets/images/drco/girl5.png'),
      require('@/assets/images/drco/girl6.png'),
      require('@/assets/images/drco/girl7.png'),
      require('@/assets/images/drco/girl8.png'),
      require('@/assets/images/drco/girl9.png'),
      require('@/assets/images/drco/girl10.png'),
      require('@/assets/images/drco/girl11.png'),
      // Social Media
      require('@/assets/images/drco/social-media-1.png'),
      require('@/assets/images/drco/social-media-2.png'),
      require('@/assets/images/drco/social-media-3.png'),
      require('@/assets/images/drco/social-media-4.png'),
      require('@/assets/images/drco/social-media-5.png'),
      require('@/assets/images/drco/social-media-6.png'),
      require('@/assets/images/drco/social-media-7.png'),
      require('@/assets/images/drco/social-media-8.png'),
      require('@/assets/images/drco/social-media-9.png'),
      require('@/assets/images/drco/social-media-10.png'),
      require('@/assets/images/drco/social-media-11.png'),
      require('@/assets/images/drco/social-media-12.png')
    ]
  },
  niiv: {
    loaded: 0,
    urls: [
      // Section 3
      require('@/assets/images/niiv/35.png'),
      require('@/assets/images/niiv/36.png'),
      require('@/assets/images/niiv/36_1.png'),
      // Section 4
      require('@/assets/images/niiv/37.png'),
      require('@/assets/images/niiv/37_1.png'),
      require('@/assets/images/niiv/38.png'),
      require('@/assets/images/niiv/39.png'),
      require('@/assets/images/niiv/40.png'),
      require('@/assets/images/niiv/41.png'),
      require('@/assets/images/niiv/42.png'),
      require('@/assets/images/niiv/43.png'),
      require('@/assets/images/niiv/45.png'),
      require('@/assets/images/niiv/46.png'),
      require('@/assets/images/niiv/sec4_1.png'),
      require('@/assets/images/niiv/sec4_2.png'),
      require('@/assets/images/niiv/sec4_3.png'),
      // Section 5
      require('@/assets/images/niiv/48.png'),
      require('@/assets/images/niiv/48_a.png'),
      require('@/assets/images/niiv/48_b.png'),
      require('@/assets/images/niiv/48_c.png'),
      require('@/assets/images/niiv/49_a.png'),
      require('@/assets/images/niiv/49_b.png'),
      require('@/assets/images/niiv/49_c.png'),
      // Section 9
      require('@/assets/images/niiv/1.png'),
      require('@/assets/images/niiv/8.png'),
      require('@/assets/images/niiv/pun.png'),
      require('@/assets/images/niiv/3.png'),
      require('@/assets/images/niiv/2.png'),
      require('@/assets/images/niiv/madeit.png'),
      require('@/assets/images/niiv/why_not.png'),
      // Section 10
      require('@/assets/images/niiv/4.png'),
      require('@/assets/images/niiv/logo.gif'),
      require('@/assets/images/niiv/5_a.png'),
      require('@/assets/images/niiv/7.png'),
      // Large images
      require('@/assets/images/niiv/27.png'),
      require('@/assets/images/niiv/12.png'),
      require('@/assets/images/niiv/31.png'),
      require('@/assets/images/niiv/29.png'),
      require('@/assets/images/niiv/19.png')
    ]
  }
}
