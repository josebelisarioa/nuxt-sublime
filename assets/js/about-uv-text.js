export default [
  // {
  //   title: 'Gallery',
  //   emit: { event: 'open-gallery', payload: true },
  //   position: {
  //     x: 1340,
  //     y: 100,
  //     w: 1465,
  //     h: 275
  //   },
  //   textLocation: {
  //     x: 0,
  //     y: 0,
  //     w: 0
  //   }
  // },
  {
    title: 'Brochure',
    position: {
      x: 105,
      y: 520,
      w: 340,
      h: 760
    },
    url: 'https://sublimestudio.la/brochure.pdf',
    textLocation: {
      x: 0,
      y: 0,
      w: 0
    }
  }
]
