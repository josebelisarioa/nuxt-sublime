export function arrayRotate(arr, count) {
  count -= arr.length * Math.floor(count / arr.length)
  arr.push.apply(arr, arr.splice(0, count))
  return arr
}

export function memoizer(fun) {
  const cache = {}
  return function(n) {
    if (cache[n] !== undefined) {
      return cache[n]
    } else {
      const result = fun(n)
      cache[n] = result
      return result
    }
  }
}

export function intToString(n) {
  switch (n) {
    case 1:
      return 'one'
    case 2:
      return 'two'
    case 3:
      return 'three'
    case 4:
      return 'four'
    case 5:
      return 'five'
    case 6:
      return 'six'
  }
}

export function stringToInt(w) {
  switch (w) {
    case 'one':
      return 1
    case 'two':
      return 2
    case 'three':
      return 3
    case 'four':
      return 4
    case 'five':
      return 5
    case 'six':
      return 6
  }
}

export function wait(time) {
  return new Promise((resolve) => setTimeout(resolve, time))
}

export function waitFor(sleep, condition, callback) {
  if (!condition()) {
    sleep(sleep).then(() => waitFor(sleep, condition, callback))
  } else {
    callback()
  }
}

export function repeatForever(sleep, condition, callback) {
  setTimeout(function() {
    callback()
    if (condition()) repeatForever(sleep, condition, callback)
  }, sleep)
}

export function createCSSSelector(selector, style) {
  if (!document.styleSheets) return
  if (document.getElementsByTagName('head').length === 0) return

  let styleSheet, mediaType

  if (document.styleSheets.length > 0) {
    for (let i = 0, l = document.styleSheets.length; i < l; i++) {
      if (document.styleSheets[i].disabled) continue
      const media = document.styleSheets[i].media
      mediaType = typeof media

      if (mediaType === 'string') {
        if (media === '' || media.indexOf('screen') !== -1) {
          styleSheet = document.styleSheets[i]
        }
      } else if (mediaType === 'object') {
        if (
          media.mediaText === '' ||
          media.mediaText.indexOf('screen') !== -1
        ) {
          styleSheet = document.styleSheets[i]
        }
      }

      if (typeof styleSheet !== 'undefined') break
    }
  }

  if (typeof styleSheet === 'undefined') {
    const styleSheetElement = document.createElement('style')
    styleSheetElement.type = 'text/css'
    document.getElementsByTagName('head')[0].appendChild(styleSheetElement)

    for (let i = 0; i < document.styleSheets.length; i++) {
      if (document.styleSheets[i].disabled) {
        continue
      }
      styleSheet = document.styleSheets[i]
    }

    mediaType = typeof styleSheet.media
  }

  if (mediaType === 'string') {
    for (let i = 0, l = styleSheet.rules.length; i < l; i++) {
      if (
        styleSheet.rules[i].selectorText &&
        styleSheet.rules[i].selectorText.toLowerCase() ===
          selector.toLowerCase()
      ) {
        styleSheet.rules[i].style.cssText = style
        return
      }
    }
    styleSheet.addRule(selector, style)
  } else if (mediaType === 'object') {
    const styleSheetLength = styleSheet.cssRules
      ? styleSheet.cssRules.length
      : 0
    for (let i = 0; i < styleSheetLength; i++) {
      if (
        styleSheet.cssRules[i].selectorText &&
        styleSheet.cssRules[i].selectorText.toLowerCase() ===
          selector.toLowerCase()
      ) {
        styleSheet.cssRules[i].style.cssText = style
        return
      }
    }
    styleSheet.insertRule(selector + '{' + style + '}', styleSheetLength)
  }
}
