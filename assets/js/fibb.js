const memoizer = require('./util').memoizer
const GOLDEN_RATIO = 0.618
const GOLDEN_RATIO_RAISED_3 = 0.236

export function setPositionAttribute(element, index, isVertical = false) {
  const calcLeft = isVertical ? calcY : calcX
  const calcTop = isVertical ? calcX : calcY
  const calcWidth = isVertical ? calcMajorAxis : calcMinorAxis
  const calcHeight = isVertical ? calcMinorAxis : calcMajorAxis

  element.style.zIndex = index
  element.style.left = porcentualize(calcLeft(index, isVertical))
  element.style.top = porcentualize(calcTop(index, isVertical))
  element.style.width = porcentualize(calcWidth(index))
  element.style.height = porcentualize(calcHeight(index))
}

const calcX = memoizer(function(index, isVertical = false) {
  const calcWidth = isVertical ? calcMajorAxis : calcMinorAxis
  const pos = index + 1
  if (pos === 1) return 0
  else if (pos === 2) return calcX(0) + calcWidth(0)
  else if (pos === 3) {
    return calcX(1) + calcWidth(1) - calcWidth(1) * GOLDEN_RATIO
  } else if (pos % 4 === 0) return calcX(index - 3) + calcWidth(index - 3)
  else if (pos % 4 === 1) return calcX(index - 4) + calcWidth(index - 4)
  else if (pos % 4 === 2) return calcX(index - 1) + calcWidth(index - 1)
  else return calcX(index - 1) + calcWidth(index - 1) - calcWidth(index)
})

const calcY = memoizer(function(index, isVertical = false) {
  const calcHeight = isVertical ? calcMinorAxis : calcMajorAxis
  const pos = index + 1
  if (pos === 1) return 0
  else if (pos === 2) return calcY(0)
  else if (pos === 3) return calcY(1) + calcHeight(1)
  else if (pos % 4 === 0) {
    return (
      calcY(index - 3) +
      calcHeight(index - 3) -
      calcHeight(index - 3) * GOLDEN_RATIO_RAISED_3
    )
  } else if (pos % 4 === 1) return calcY(index - 3) + calcHeight(index - 3)
  else if (pos % 4 === 2) return calcY(index - 1)
  else return calcY(index - 1) + calcHeight(index - 1)
})

const calcMajorAxis = memoizer(function(index) {
  return Math.pow(GOLDEN_RATIO, index)
})

const calcMinorAxis = memoizer(function(index) {
  return Math.pow(GOLDEN_RATIO, index + 1)
})

function porcentualize(decimal) {
  return decimal * 100 + '%'
}
