import Cookie from 'js-cookie'

export const MESSAGE_DELIVERED = (state, value) => {
  state.delivered = value
}
export const PLAY_GLITCH = (state, value) => {
  state.playGlitch = value
}
export const SET_ACTIVE_PROJECT = (state, value) => {
  state.project = value
}
export const SET_DARK_THEME = (state) => {
  state.isDark = true
  Cookie.set('theme', 'dark', { expires: 365 })
}
export const SET_LAYOUT_VISIBLE = (state, value) => {
  state.isLayoutVisible = value
}
export const SET_LIGHT_THEME = (state) => {
  state.isDark = false
  Cookie.set('theme', 'light', { expires: 365 })
}
export const SET_LOGO_VISIBLE = (state, bool) => {
  state.logoZIndex = bool ? 8 : -1
}
export const SET_MASK_COLOR = (state, payload) => {
  state.maskColor = payload.color.replace(/[^,]+(?=\))/, payload.alpha)
}
export const SET_MASK_VISIBLE = (state, value) => {
  state.isTransitioning = value
}
export const SET_MENU_VISIBLE = (state, value) => {
  state.isMenuVisible = value
}
export const SET_PRIORITY = (state, value) => {
  state.priority = value
  Cookie.set('priority', value, { expires: 365 })
}
export const SET_THIRD_ACTION = (state, payload) => {
  state.thirdAction = payload
}
export const SET_WINDOW_SIZE = (state, value) => {
  state.isVertical = value < 992
}
export const TOGGLE_CHECK = (state) => {
  state.isToggled = !state.isToggled
}
export const TOGGLE_MENU = (state) => {
  state.isMenuVisible = !state.isMenuVisible
}
