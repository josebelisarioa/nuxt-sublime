import Vuex from 'vuex'
import Cookie from 'js-cookie'
import * as mutations from './mutations'
import * as Color from '@/assets/js/colors'

const isDark = Cookie.get('theme') !== 'light'
const priority = Cookie.get('priority') === 'process' ? 'process' : 'results'

const createStore = () => {
  return new Vuex.Store({
    state: {
      delivered: false,
      isDark,
      isLayoutVisible: false,
      isMenuVisible: false,
      isVertical: false,
      logoZIndex: 8,
      maskColor: undefined,
      playGlitch: false,
      glitchDuration: 1000,
      priority
    },
    getters: {
      activeColor: (state) => {
        return state.isDark ? Color.Black : Color.White
      },
      absoluteColor: (state) => {
        return state.isDark ? Color.AbsoluteBlack : Color.AbsoluteWhite
      },
      accentColor: (state) => {
        return state.isDark ? Color.AccentResults : Color.AccentProcess
      },
      maskDepth: (state) => {
        return state.isTransitioning ? 9 : -1
      },
      layoutOpacity: (state) => {
        return state.isLayoutVisible ? 1 : 0
      }
    },
    mutations
  })
}

export default createStore
