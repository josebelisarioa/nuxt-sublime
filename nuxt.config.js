const isProd = process.NODE_ENV === 'production'
const mode = isProd ? 'universal' : 'spa'

export default {
  mode,
  /*
   ** Headers of the page
   */
  head: {
    title: 'Sublime Studio',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      // {
      //   hid: 'description',
      //   name: 'description',
      //   content: process.env.npm_package_description || ''
      // }
    ],
    script: [
      {
        src: isProd
          ? 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js'
          : '/js/jquery-3.4.1.min.js',
        defer: true
      },
      {
        src: '/js/bx-slider-modified.min.js',
        defer: true
      },
      {
        src: isProd
          ? 'https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js'
          : '/js/slick.min.js',
        defer: true
      }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/faviconv3.ico' },
      {
        rel: 'stylesheet',
        type: 'text/css',
        href: isProd
          ? 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css'
          : '/css/bootstrap.min.css',
        integrity:
          'sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm',
        crossorigin: 'anonymous'
      },
      {
        rel: 'stylesheet',
        type: 'text/css',
        href: isProd
          ? 'https://cdnjs.cloudflare.com/ajax/libs/bxslider/4.2.15/jquery.bxslider.min.css'
          : '/css/jquery.bxslider.min.css'
      },
      {
        rel: 'stylesheet',
        type: 'text/css',
        href: isProd
          ? 'https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css'
          : '/css/slick.css'
      }
    ]
  },
  /*
   ** Customize the progress-bar color
   */
  loading: { color: '#fff' },
  /*
   ** Global CSS
   */
  css: [
    '@/assets/css/main.css',
    '@/assets/css/style.css',
    '@/assets/css/video.css',
    '@/assets/css/slick-theme.scss'
  ],
  /*
   ** Plugins to load before mounting the App
   */
  plugins: [],
  /*
   ** Nuxt.js modules
   */
  modules: [
    '@nuxtjs/pwa',
    '@nuxtjs/eslint-module',
    '@nuxtjs/style-resources',
    '@bazzite/nuxt-optimized-images'
  ],
  optimizedImages: {
    optimizeImages: true
  },
  /*
   ** Build configuration
   */
  build: {
    loaders: {
      vue: {
        transformAssetUrls: {
          audio: 'src'
        }
      }
    },

    extend(config, ctx) {
      config.module.rules.push({
        test: /\.(ogg|mp3|wav|mpe?g)$/i,
        loader: 'file-loader',
        options: {
          name: '[path][name].[ext]'
        }
      })
    }
  },
  serverMiddleware: ['@/api/contact.js']
}
